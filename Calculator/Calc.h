#pragma once

#include <iomanip>
#include <string>
#include <iostream>
#include <cmath>
#include <algorithm>


class Calc
{
public:
	Calc();
	~Calc();

	std::string PEMDAS(std::string input);


private:
	std::string operators = "^*/+-";
};

