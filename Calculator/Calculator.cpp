// Calculator.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Calc.h"
#include <iomanip>
#include <string>
#include <iostream>

using namespace std;


int main()
{
	Calc obj;
	string input;

	do
	{
		cout << "Just press Enter to exit." << endl << endl;

		getline(cin, input);

		if (input == "")
		{
			break;
		}

		std::string output = obj.PEMDAS(input);

		cout << output << endl << endl;

	} while (input != "");

	return 0;
}

