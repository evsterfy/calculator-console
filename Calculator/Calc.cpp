#include "stdafx.h"
#include "Calc.h"

using namespace std;


Calc::Calc()
{
}


Calc::~Calc()
{
}


std::string Calc::PEMDAS(std::string input)
{
	double op1 = 0;
	double op2 = 0;

	int count = 0;
	int q = 0;
	int prec = 0;
	int index = 0;
	bool opp = false;
	int len = 0;
	int decimal = 0;
	int end = 0;
	int start = 0;
	std::string operand = "";
	int p = 0;
	int f = 0;
	bool para = false;
	int i = 0;
	int r = 0;

	char oprt1 = ' ';
	char oprt2 = ' ';
	char oprt3 = ' ';
	char oprt4 = ' ';


	// r is index of operators string, automatic precedence with for loop.
	// i is for loop iterator, I check if there's no more operators in input and set it to -1 so it breaks all loops.
	while (r >= 0 && i >= 0)
	{
		// if we get out into this while loop, we check if there's any operators and reset r, else we break and return input.
		if (input.find(operators[0]) >= 0 || input.find(operators[1]) >= 0 || input.find(operators[2]) >= 0 ||
			input.find(operators[3]) >= 0 || input.find(operators[4]) >= 0)
		{
			r = 0;
		}
		else
		{
			break;
		}

		// loop until we reach end of operators string, then break out to previous while
		while (r < 5 && i >= 0)
		{
			// using this loop just because, could have used a while and incremented i myself but idk.
			for (i = 0; i < input.length(); i++)
			{
				// left to right
				p = input.find_first_of(')');
				f = p;

				// iterate backwards through input until it finds an opening parenthesis
				if (f >= 0)
				{
					while (input[f] != '(')
					{
						f--;
					}
				}

				// if we find an opening parenthesis
				if (f >= 0)
				{
					// find first operator we are working with past the opening parenthesis
					i = input.find_first_of(operators[r], f);

					// find if there are any other operators
					int zero = input.find_first_of(operators[0], f);
					int one = input.find_first_of(operators[1], f);
					int two = input.find_first_of(operators[2], f);
					int three = input.find_first_of(operators[3], f);
					int four = input.find_first_of(operators[4], f);

					int temp0 = zero;
					int temp1 = one;
					int temp2 = two;
					int temp3 = three;
					int temp4 = four;

					// if we find any other operators and they are at the same index as i
					// look for any next operator, top to bottom precedence.
					if (zero == i && zero >= 0)
					{
						zero = input.find_first_of(operators[0], i + 1);

						if (zero < 0)
						{
							zero = temp0;
						}
					}
					if (one == i && one >= 0)
					{
						one = input.find_first_of(operators[1], i + 1);

						if (one < 0)
						{
							zero = temp1;
						}
					}
					if (two == i && two >= 0)
					{
						two = input.find_first_of(operators[2], i + 1);

						if (two < 0)
						{
							zero = temp2;
						}
					}
					if (three == i && three >= 0)
					{
						three = input.find_first_of(operators[3], i + 1);

						if (three < 0)
						{
							zero = temp3;
						}
					}
					if (four == i && four >= 0)
					{
						four = input.find_first_of(operators[4], i + 1);

						if (four < 0)
						{
							zero = temp4;
						}
					}

					if (i >= 0 && i < p)
					{
						if (((zero != i) && (zero < p) && (zero >= 0)) || ((one != i) && (one < p) && (one >= 0)) ||
							((two != i) && (two < p) && (two >= 0)) || ((three != i) && (three < p) && (three >= 0)) ||
							((four != i) && (four < p) && (four >= 0)))
						{
							para = true;

							if (!para)
							{
								input.erase(f, 1);
								input.erase(p - 1, 1);
								i--;
							}
						}
						else
						{
							para = false;

							if (!para)
							{
								input.erase(f, 1);
								input.erase(p - 1, 1);
								i--;
							}
						}
					}
					else if (i >= 0 && i > p)
					{
						if (!para)
						{
							input.erase(f, 1);
							input.erase(p - 1, 1);
							i--;
						}
					}
				}
				else
				{
					i = input.find_first_of(operators[r]);
				}

				int zeroCheck = input.find(operators[0]);
				int oneCheck = input.find(operators[1]);
				int twoCheck = input.find(operators[2]);
				int threeCheck = input.find(operators[3]);
				int fourCheck = input.find_last_of(operators[4]);

				if (i < 0 && zeroCheck < 0 && oneCheck < 0 && twoCheck < 0 && threeCheck < 0)
				{
					if (fourCheck == 0)
					{
						i = -1;
						break;
					}
					else if (fourCheck < 0)
					{
						return input;
					}
				}

				// didn't find current operator in string, leave nested for, iterate outer for.
				if (i < 0)
				{
					i = input.length();
					continue;
				}

				opp = false; // for different switch cases based on left to right precedence.

				// * and / and also + and - are same prec so we have to make sure and do / or - if they come first.
				if (operators[r + 1] == '/' || operators[r + 1] == '-')
				{
					q = input.find_first_of(operators[r + 1]);
					prec = min(i, q);

					if (prec > 0)
					{
						if (min(i, q) == q)
						{
							i = prec;
							opp = true;
						}
					}
				}
				else
				{
					prec = -1;
					opp = false;
				}

				// just for safety really
				if (input[i] == operators[r] || input[i] == operators[r + 1])
				{
					int k = i;
					int m = r;

					// wanted to make this func as simple as possible, so this created a way to store the other 4 operators
					// in variables so we can just iterate through all this code with any current operator.
					//
					// operators string length is 5 so basically just increment it, wrapping around
					if (m < 4)
					{
						m++;
					}
					else
					{
						m = 0;
					}

					// if opp is true, meaning we are doing - before + or / before *, increment again
					if (m < 4 && opp)
					{
						m++;
					}
					else if (m == 4 && opp)
					{
						m = 0;
					}

					count = 0; // counting the iterations through below while, then breaking when 4

					while (count != 4)
					{
						count++;

						// just store the operators we aren't solving from operators string in variables.
						switch (count)
						{
						case 1:
							oprt1 = operators[m];
							break;

						case 2:
							oprt2 = operators[m];
							break;

						case 3:
							oprt3 = operators[m];
							break;

						case 4:
							oprt4 = operators[m];
							break;
						}

						// make sure to increment each time so we get new operator

						if (m == 4)
						{
							m = 0;
						}
						else
						{
							m++;
						}
					}

					// working on negative number support.
					if (input[0] == '-')
					{
						for (int h = 1; h < input.length(); h++)
						{
							if (input[h] == oprt1 || input[h] == oprt2 || input[h] == oprt3 || input[h] == oprt4 || input[h] == operators[r])
							{
								break;
							}
							else if (h == input.length() - 1)
							{
								return input;
							}
						}
					}


					// k is just a copy of i, so we can change it and increment through 'input'. Going right of operator.
					while (k != input.length() && input[k] != oprt1 && input[k] != oprt2
						&& input[k] != oprt3 && input[k] != oprt4 && input[k] != ' ' && input[k] != ')')
					{
						k++;

						// k is out of range or input[k] is one of the operators were working with.
						if (k == input.length() || input[k] == operators[r] || input[k] == operators[r + 1])
						{
							break;
						}
					}

					if (input[k] == '-' && (input[k - 1] == oprt1 || input[k - 1] == oprt2
						|| input[k - 1] == oprt3 || input[k - 1] == oprt4 || input[k - 1] == ' '
						|| input[k - 1] == ')' || input[k - 1] == '(' || input[k - 1] == operators[r]))
					{
						k++;

						while (k != input.length() && input[k] != oprt1 && input[k] != oprt2
							&& input[k] != oprt3 && input[k] != oprt4 && input[k] != ' ' && input[k] != ')')
						{
							k++;

							// k is out of range or input[k] is one of the operators were working with.
							if (k == input.length() || input[k] == operators[r] || input[k] == operators[r + 1])
							{
								break;
							}
						}
					}

					op2 = stod(input.substr((i + 1), (k - 1))); // (i + 1) is first digit after operator were working with.
																// (k -1 ) is last
					if (input[k] == ')' && !para)
					{
						end = k + 1;
					}
					else
					{
						end = k; // used to calc the length of the input string we are replacing with new calculation
					}

					k = i; // reset k and go left

					// Going left.
					while (k >= 0 && input[k] != oprt1 && input[k] != oprt2
						&& input[k] != oprt3 && input[k] != oprt4 && input[k] != ' ' 
						&& input[k] != '(')
					{
						k--;

						if (k == -1 || input[k] == operators[r] || input[k] == operators[r + 1])
						{
							break;
						}
					}

					// Weird bug with some equations, idk how it works but if we are replacing a substr starting at 0,
					// we have to use 0 and i rather than k + 1 and i - 1, i tried it a couple times and just did this so whatever.
					if (k == -1)
					{
						op1 = stod(input.substr(0, i));
					}
					else if (input[k] == '-')
					{
						op1 = stod(input.substr(k, i));
					}
					else
					{
						op1 = stod(input.substr((k + 1), (i - 1)));
					}

					if (k >= 0 && input[k] == '(' && !para)
					{
						start = k;
					}
					else if (op1 < 0)
					{
						start = k;
					}
					else
					{
						start = (k + 1); // start of substr were replacing
					}

					k = i; // pointless


					switch (opp)
					{
						// if opp is true, we are working with precedence, so test what operator we are using.
					case true:
					{
						switch (input[prec])
						{
						case '^':
							operand = std::to_string(pow(op1, op2)); // pointless
							break;

						case '*':
							operand = std::to_string(op1 * op2);
							break;

						case '/':
							operand = std::to_string(op1 / op2);
							break;

						case '+':
							operand = std::to_string(op1 + op2);
							break;

						case '-':
							operand = std::to_string(op1 - op2);
							break;
						}
						break;
					}

					// not working with precedence, just test what the current operator is in op[r]
					case false:
					{
						switch (operators[r])
						{
						case '^':
							operand = std::to_string(pow(op1, op2));
							break;

						case '*':
							operand = std::to_string(op1 * op2);
							break;

						case '/':
							operand = std::to_string(op1 / op2);
							break;

						case '+':
							operand = std::to_string(op1 + op2);
							break;

						case '-':
							operand = std::to_string(op1 - op2);
							break;
						}
						break;
					}
					}


					// find the decimal point in the new operand.
					decimal = operand.find('.');

					// if decimal is found, cut off at 2 decimal places. And if the two remaining places are 0, just erase.
					if (decimal >= 0)
					{
						operand.erase(decimal + 3);

						if (operand[decimal + 1] == '0' && operand[decimal + 2] == '0')
						{
							operand.erase(decimal);
						}
					}

					// length of substr to replace.
					len = end - start;

					input.replace(start, len, operand); // replace it!

					if (input.find(operators[0]) >= 0 || input.find(operators[1]) >= 0 || input.find(operators[2]) >= 0 ||
						input.find(operators[3]) >= 0 || input.find(operators[4]) >= 0)
					{
						i = 0;
						r = 0;
					}

					// Now loop until we test all operators.
				}
			}

			r++;

			if (i == input.length())
			{
				i = -1;
			}
		}
	}
	return input;
}